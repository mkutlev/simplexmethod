package iopp.simplex;

public class Matrix {
	public float[][] A;
	private int size;
	private float[] temp;

	public Matrix(int paramInt) {
		this.A = new float[paramInt][paramInt];
		this.temp = new float[paramInt];
		this.size = paramInt;
	}

	public boolean solve(float[] paramArrayOfFloat1, float[] paramArrayOfFloat2) {
		int i;
		for (int j = 0; j < this.size - 1; j++) {
			float f1 = Math.abs(this.A[j][j]);
			int n = j;
			for (int m = j + 1; m < this.size; m++) {
				if (Math.abs(this.A[m][j]) > f1) {
					f1 = Math.abs(this.A[m][j]);
					n = m;
				}

			}

			if (n != j) {
				float f3 = paramArrayOfFloat2[n];
				paramArrayOfFloat2[n] = paramArrayOfFloat2[j];
				paramArrayOfFloat2[j] = f3;
				for (int m = 0; m < this.size; m++) {
					f3 = this.A[n][m];
					this.A[n][m] = this.A[j][m];
					this.A[j][m] = f3;
				}

			}

			if (this.A[j][j] != 0.0F) {
				for (i = j + 1; i < this.size; i++) {
					float f2 = this.A[i][j] / this.A[j][j];
					paramArrayOfFloat2[i] -= f2 * paramArrayOfFloat2[j];
					for (int k = j; k < this.size; k++) {
						this.A[i][k] -= f2 * this.A[j][k];
					}

				}

			}

		}

		for (int j = this.size - 1; j >= 0; j--) {
			paramArrayOfFloat1[j] = paramArrayOfFloat2[j];
			for (i = this.size - 1; i > j; i--) {
				paramArrayOfFloat1[j] -= paramArrayOfFloat1[i] * this.A[j][i];
			}
			if (this.A[j][j] != 0.0F) {
				paramArrayOfFloat1[j] /= this.A[j][j];
			}
		}
		return true;
	}
}