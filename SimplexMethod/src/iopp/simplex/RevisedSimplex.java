package iopp.simplex;

public class RevisedSimplex {
	public int numVariables;
	public int numConstraints;
	public int numNonbasic;
	public int numConstraintsSoFar = 0;
	public int CurrentStep = 0;
	public int NumIterations = 0;
	public int NumArtificials;
	private int i;
	private int j;
	private int k;
	private int l;
	public float[] reducedCost;
	public float[] cost;
	public float[] x;
	public float[] pi;
	public float[] yB;
	public float MinRatio;
	public int NumMinRatio;
	public Matrix Bt;
	public Matrix B;
	public float[] littleCost;
	public float objectiveValue = 0.0F;
	public float[][] A;
	public float[] b;
	public int[] constraintType;
	public int[] BasicVariables;
	public int[] NonBasicVariables;
	public int[] varType;
	public float[] colOfA;
	int[] perm;
	public int EnteringVariable;
	public int LeavingVariable;
	public boolean TypeMinimize;
	boolean ArtificialAdded = false;

	final int LessThan = 0;
	final int GreaterThan = 1;
	final int EqualTo = 2;

	final int Continue = 0;
	final int Optimal = 1;
	final int Feasible = 2;
	final int Unbounded = 3;

	final int Regular = 0;
	final int SlackOrSurplus = 1;
	final int Artificial = 2;

	final int BasicType = 1;
	final int NonBasicType = 2;
	boolean oldOptType;
	float[] OriginalCost;

	public RevisedSimplex(int paramInt1, int paramInt2) {
		this.numVariables = paramInt1;
		this.numConstraints = paramInt2;
		this.NumArtificials = 0;
		this.reducedCost = new float[paramInt1 + 2 * paramInt2];
		this.cost = new float[paramInt1 + 2 * paramInt2];
		this.OriginalCost = new float[paramInt1 + 2 * paramInt2];
		this.x = new float[paramInt2];
		this.pi = new float[paramInt2];
		this.yB = new float[paramInt2];
		this.littleCost = new float[paramInt2];
		this.A = new float[paramInt2][paramInt1 + 3 * paramInt2];
		this.b = new float[paramInt2];
		this.constraintType = new int[paramInt2];
		this.BasicVariables = new int[paramInt2];
		this.NonBasicVariables = new int[paramInt1 + 2 * paramInt2];
		this.varType = new int[paramInt1 + 2 * paramInt2];
		this.colOfA = new float[paramInt2];
		this.Bt = new Matrix(paramInt2);
		this.B = new Matrix(paramInt2);
	}

	public int augmentBasis(int paramInt) {
		int i5 = this.numConstraints;
		int i6 = this.numVariables;

		if (i5 > i6) {
			System.out.println("Odd-shaped constraint matrix:");
			System.out.println("rows = " + i5 + " cols = " + i6);
			return 1;
		}

		if (paramInt > i5) {
			System.out.println("Too many elements in the basis!");
			System.out.println("BasisSize = " + paramInt + " rows = " + i5);
			return 1;
		}
		if ((paramInt < 0) || (i5 <= 0) || (i6 <= 0)) {
			System.out.println("Something is rotten in the State of Denmark");
			return 1;
		}

		for (int m = 0; m < paramInt; m++) {
			if ((this.BasicVariables[m] < 0) || (this.BasicVariables[m] >= i6)) {
				System.out.println("Basis element[" + m + "]= "
						+ this.BasicVariables[m] + " out of range");
				return 1;
			}
		}

		if (paramInt == i5) {
			System.out
					.println("Basis already has the right number of elements");
			return 0;
		}

		this.perm = new int[i6];
		float[][] arrayOfFloat = new float[i5][i6];

		int i3 = 0;
		int i2 = i6 - 1;
		int n;
		for (int m = 0; m < i6; m++) {
			if (inbasis(m, paramInt, this.BasicVariables)) {
				for (n = 0; n < i5; n++)
					arrayOfFloat[n][i3] = this.A[n][m];
				this.perm[i3] = m;
				i3++;
			} else {
				for (n = 0; n < i5; n++)
					arrayOfFloat[n][i2] = this.A[n][m];
				this.perm[i2] = m;
				i2--;
			}
		}

		float[] arrayOfFloat1 = new float[i5];
		float f1;
		float f2;
		int i1;
		float f3;
		for (int m = 0; m < paramInt; m++) {
			f1 = 0.0F;
			for (n = m; n < i5; n++)
				f1 += arrayOfFloat[n][m] * arrayOfFloat[n][m];
			f1 = (float) Math.sqrt(f1);

			for (n = m; n < i5; n++) {
				arrayOfFloat1[n] = arrayOfFloat[n][m];
			}
			if (arrayOfFloat1[m] < 0.0F) {
				arrayOfFloat[m][m] = f1;
				arrayOfFloat1[m] -= f1;
			} else {
				arrayOfFloat[m][m] = (-f1);
				arrayOfFloat1[m] += f1;
			}

			for (n = m + 1; n < i5; n++) {
				arrayOfFloat[n][m] = 0.0F;
			}

			f2 = 0.0F;
			for (n = m; n < i5; n++) {
				f2 += arrayOfFloat1[n] * arrayOfFloat1[n];
			}
			f2 = 2.0F / f2;

			for (i1 = m + 1; i1 < i6; i1++) {
				f3 = 0.0F;
				for (n = m; n < i5; n++)
					f3 += arrayOfFloat1[n] * arrayOfFloat[n][i1];
				f3 *= f2;
				for (n = m; n < i5; n++) {
					arrayOfFloat[n][i1] -= f3 * arrayOfFloat1[n];
				}

			}

		}

		for (int m = paramInt; m < i5; m++) {
			f2 = Math.abs(arrayOfFloat[m][m]);
			i2 = m;
			for (n = m + 1; n < i6; n++) {
				if (Math.abs(arrayOfFloat[m][n]) > f2) {
					f2 = Math.abs(arrayOfFloat[m][n]);
					i2 = n;
				}
			}

			int i4 = this.perm[m];
			this.perm[m] = this.perm[i2];
			this.perm[i2] = i4;

			for (n = 0; n < i5; n++) {
				float f4 = arrayOfFloat[n][m];
				arrayOfFloat[n][m] = arrayOfFloat[n][i2];
				arrayOfFloat[n][i2] = f4;
			}

			if (m < i5 - 1) {
				f1 = 0.0F;
				for (n = m; n < i5; n++) {
					f1 += arrayOfFloat[n][m] * arrayOfFloat[n][m];
				}
				f1 = (float) Math.sqrt(f1);

				for (n = m; n < i5; n++) {
					arrayOfFloat1[n] = arrayOfFloat[n][m];
				}
				if (arrayOfFloat1[m] < 0.0F) {
					arrayOfFloat[m][m] = f1;
					arrayOfFloat1[m] -= f1;
				} else {
					arrayOfFloat[m][m] = (-f1);
					arrayOfFloat1[m] += f1;
				}

				for (n = m + 1; n < i5; n++) {
					arrayOfFloat[n][m] = 0.0F;
				}

				f2 = 0.0F;
				for (n = m; n < i5; n++)
					f2 += arrayOfFloat1[n] * arrayOfFloat1[n];
				f2 = 2.0F / f2;

				for (i1 = m + 1; i1 < i6; i1++) {
					f3 = 0.0F;
					for (n = m; n < i5; n++)
						f3 += arrayOfFloat1[n] * arrayOfFloat[n][i1];
					f3 *= f2;
					for (n = m; n < i5; n++) {
						arrayOfFloat[n][i1] -= f3 * arrayOfFloat1[n];
					}

				}

			}

		}

		return 0;
	}

	public void chooseEnteringVariable() {
		int m = 0;
		float f = 100000.0F;

		for (this.i = 0; this.i < this.numNonbasic; this.i += 1) {
			if ((this.reducedCost[this.i] < 0.0F)
					&& (this.reducedCost[this.i] < f)) {
				m = this.i;
				f = this.reducedCost[this.i];
			}
		}
		this.EnteringVariable = m;
	}

	public float dot(float[] paramArrayOfFloat1, float[] paramArrayOfFloat2,
			int paramInt) {
		float f = 0.0F;
		for (int m = 0; m < paramInt; m++)
			f += paramArrayOfFloat1[m] * paramArrayOfFloat2[m];
		return f;
	}

	public void setCostForPhaseOne() {
		float[] arrayOfFloat = new float[this.numVariables];

		for (int m = 0; m < this.numVariables; m++) {
			this.OriginalCost[m] = this.cost[m];

			if (this.varType[m] == 2)
				arrayOfFloat[m] = 1.0F;
			else
				arrayOfFloat[m] = 0.0F;
		}
		specifyObjective(arrayOfFloat, true);
	}

	public void addConstraint(float[] paramArrayOfFloat, float paramFloat,
			int paramInt) {
		for (this.i = 0; this.i < this.numVariables; this.i += 1) {
			this.A[this.numConstraintsSoFar][this.i] = paramArrayOfFloat[this.i];
		}
		this.x[this.numConstraintsSoFar] = paramFloat;
		this.b[this.numConstraintsSoFar] = paramFloat;

		this.constraintType[this.numConstraintsSoFar] = paramInt;
		this.numConstraintsSoFar += 1;
	}

	public float calculateObjective() {
		float f = 0.0F;
		int m;
		if (this.TypeMinimize == true)
			for (m = 0; m < this.numConstraints; m++)
				f += this.x[m] * this.cost[this.BasicVariables[m]];
		else {
			for (m = 0; m < this.numConstraints; m++)
				f -= this.x[m] * this.cost[this.BasicVariables[m]];
		}
		return f;
	}

	public void calculateReducedCosts() {
		for (this.i = 0; this.i < this.numNonbasic; this.i += 1) {
			for (this.j = 0; this.j < this.numConstraints; this.j += 1) {
				this.colOfA[this.j] = this.A[this.j][this.NonBasicVariables[this.i]];
			}
			this.reducedCost[this.i] = (this.cost[this.NonBasicVariables[this.i]] - dot(
					this.pi, this.colOfA, this.numConstraints));
		}
	}

	public void chooseLeavingVariable() {
		int m = -1;

		this.NumMinRatio = 0;

		for (this.i = 0; this.i < this.numConstraints; this.i += 1) {
			if (this.yB[this.i] > 0.0F) {
				float f = this.x[this.i] / this.yB[this.i];
				if (this.NumMinRatio == 0) {
					this.MinRatio = f;
					m = this.i;
					this.NumMinRatio = 1;
				} else if (f < this.MinRatio) {
					this.MinRatio = f;
					m = this.i;
					this.NumMinRatio = 1;
				} else if (f == this.MinRatio) {
					this.NumMinRatio += 1;
				}
			}
		}
		this.LeavingVariable = m;
	}

	public void getRidOfArtificials() {
		int m = 0;
		int n = 0;
		int i1 = 0;

		int[] arrayOfInt = new int[this.numVariables];
		float[] arrayOfFloat = new float[this.numVariables];

		for (int i3 = 0; i3 < this.numNonbasic; i3++) {
			arrayOfInt[this.NonBasicVariables[i3]] = 2;
		}
		for (int i3 = 0; i3 < this.numConstraints; i3++) {
			arrayOfInt[this.BasicVariables[i3]] = 1;
			arrayOfFloat[this.BasicVariables[i3]] = this.x[i3];
		}

		for (int i3 = 0; i3 < this.numVariables; i3++)
			if (this.varType[i3] != 2) {
				switch (arrayOfInt[i3]) {
				case 1:
					this.BasicVariables[m] = i3;
					this.x[m] = arrayOfFloat[i3];
					m++;
					break;
				case 2:
					this.NonBasicVariables[n] = i3;
					n++;
					break;
				default:
					System.out
							.println("Error:Variable must be of Basic or NonBasic type");

					break;
				}

			} else {
				i1++;
			}
		int i2 = 0;

		for (int i3 = 0; i3 < this.numVariables; i3++) {
			if (this.varType[i3] == 2) {
				switch (arrayOfInt[i3]) {
				case 1:
					i2++;
					this.BasicVariables[m] = i3;
					this.x[m] = arrayOfFloat[i3];
					m++;
					break;
				case 2:
					this.NonBasicVariables[n] = i3;
					n++;
					break;
				default:
					System.out
							.println("Error:Variable must be of Basic or NonBasic type");
				}
			}

		}

		if (i2 > 0) {
			augmentBasis(this.numConstraints - i2);

			for (int i3 = 0; i3 < this.numConstraints; i3++) {
				this.BasicVariables[i3] = this.perm[i3];
			}

			for (int i3 = 0; i3 < this.numVariables; i3++) {
				arrayOfInt[i3] = 2;
			}
			for (int i3 = 0; i3 < this.numConstraints; i3++) {
				arrayOfInt[this.BasicVariables[i3]] = 1;
			}
			m = 0;
			n = 0;

			for (int i3 = 0; i3 < this.numVariables; i3++) {
				switch (arrayOfInt[i3]) {
				case 1:
					if (this.varType[i3] == 2) {
						System.out.println("Error in GetRidOfArtificials:");
						System.out
								.println(" There are still artificials after AugmentBasis");
					}
					this.BasicVariables[m] = i3;
					this.x[m] = arrayOfFloat[i3];
					m++;
					break;
				case 2:
					this.NonBasicVariables[n] = i3;
					n++;
					break;
				default:
					System.out
							.println("Error:Variable must be of Basic or NonBasic type");
				}
			}
		}
		this.ArtificialAdded = false;

		specifyObjective(this.OriginalCost, this.oldOptType);

		this.numNonbasic -= i1;
		this.numVariables -= i1;

		this.TypeMinimize = this.oldOptType;

		this.CurrentStep = 0;
	}

	boolean inbasis(int paramInt1, int paramInt2, int[] paramArrayOfInt) {
		for (int m = 0; m < paramInt2; m++) {
			if (paramArrayOfInt[m] == paramInt1)
				return true;
		}
		return false;
	}

	public int iterate() {
		this.NumIterations += 1;
		makeBt();
		this.Bt.solve(this.pi, this.littleCost);
		calculateReducedCosts();

		if (!testForOptimality()) {
			chooseEnteringVariable();
		} else {
			this.objectiveValue = calculateObjective();
			return 1;
		}
		makeB();

		for (int m = 0; m < this.numConstraints; m++) {
			this.colOfA[m] = this.A[m][this.NonBasicVariables[this.EnteringVariable]];
		}
		this.B.solve(this.yB, this.colOfA);

		if (!testUnboundedness()) {
			chooseLeavingVariable();
			updateSolution();

			return 0;
		}

		return 3;
	}

	public int iterateOneStep() {
		switch (this.CurrentStep) {
		case 0:
			this.NumIterations += 1;
			makeBt();
			makeB();
			this.CurrentStep = 1;
			return 0;
		case 1:
			this.Bt.solve(this.pi, this.littleCost);
			this.CurrentStep = 2;
			return 0;
		case 2:
			calculateReducedCosts();
			this.CurrentStep = 3;
			return 0;
		case 3:
			if (testForOptimality()) {
				this.CurrentStep = 12;
				return 1;
			}

			this.CurrentStep = 4;
			chooseEnteringVariable();
			return 0;
		case 4:
			this.CurrentStep = 5;
			return 0;
		case 5:
			for (int m = 0; m < this.numConstraints; m++) {
				this.colOfA[m] = this.A[m][this.NonBasicVariables[this.EnteringVariable]];
			}
			this.B.solve(this.yB, this.colOfA);
			this.CurrentStep = 6;
			return 0;
		case 6:
			if (testUnboundedness()) {
				this.CurrentStep = 7;
				return 3;
			}

			this.CurrentStep = 8;
			return 0;
		case 7:
		case 8:
			chooseLeavingVariable();
			if (this.NumMinRatio == 1)
				this.CurrentStep = 11;
			else
				this.CurrentStep = 9;
			return 0;
		case 9:
			this.CurrentStep = 10;
			return 0;
		case 10:
			this.CurrentStep = 11;
			return 0;
		case 11:
			updateSolution();
			this.objectiveValue = calculateObjective();
			this.CurrentStep = 0;
			return 0;
		case 12:
			this.objectiveValue = calculateObjective();
			return 1;
		}
		return 4;
	}

	private void makeB() {
		for (this.i = 0; this.i < this.numConstraints; this.i += 1)
			for (this.j = 0; this.j < this.numConstraints; this.j += 1)
				this.B.A[this.i][this.j] = this.A[this.i][this.BasicVariables[this.j]];
	}

	private void makeBt() {
		for (this.i = 0; this.i < this.numConstraints; this.i += 1) {
			this.littleCost[this.i] = this.cost[this.BasicVariables[this.i]];

			for (this.j = 0; this.j < this.numConstraints; this.j += 1)
				this.Bt.A[this.i][this.j] = this.A[this.j][this.BasicVariables[this.i]];
		}
	}

	public boolean preprocess(int paramInt1, int paramInt2) {
		int[] arrayOfInt = new int[paramInt2];

		this.oldOptType = this.TypeMinimize;
		int m = paramInt1;

		if (this.TypeMinimize == false) {
			for (this.i = 0; this.i < m; this.i += 1)
				this.cost[this.i] *= -1.0F;
		}
		for (this.i = 0; this.i < m; this.i += 1) {
			this.NonBasicVariables[this.i] = this.i;
		}
		int n = m;

		for (this.i = 0; this.i < paramInt2; this.i += 1) {
			switch (this.constraintType[this.i]) {
			case 0:
				this.A[this.i][m] = 1.0F;
				this.cost[m] = 0.0F;
				this.varType[m] = 1;
				arrayOfInt[this.i] = m;
				m++;
				break;
			case 1:
				this.A[this.i][m] = -1.0F;
				this.cost[m] = 0.0F;
				this.varType[m] = 1;
				arrayOfInt[this.i] = m;
				m++;
			case 2:
			}

		}

		for (this.i = 0; this.i < paramInt2; this.i += 1) {
			switch (this.constraintType[this.i]) {
			case 1:
				if (this.b[this.i] > 0.0F) {
					this.A[this.i][m] = 1.0F;
					this.x[this.i] = this.b[this.i];
					this.varType[m] = 2;
					this.ArtificialAdded = true;
					this.BasicVariables[this.i] = m;

					int i2 = arrayOfInt[this.i];
					this.NonBasicVariables[n] = i2;

					n++;
					m++;
					this.NumArtificials += 1;
				} else {
					this.BasicVariables[this.i] = arrayOfInt[this.i];
					this.x[this.i] = (-this.b[this.i]);
				}
				break;
			case 2:
				if (this.b[this.i] >= 0.0F) {
					this.A[this.i][m] = 1.0F;
					this.x[this.i] = this.b[this.i];
				} else {
					this.A[this.i][m] = -1.0F;
					this.x[this.i] = (-this.b[this.i]);
				}

				this.varType[m] = 2;
				this.ArtificialAdded = true;
				this.BasicVariables[this.i] = m;
				m++;
				this.NumArtificials += 1;
				break;
			case 0:
				if (this.b[this.i] >= 0.0F) {
					this.BasicVariables[this.i] = arrayOfInt[this.i];
					this.x[this.i] = this.b[this.i];
				} else {
					this.A[this.i][m] = -1.0F;
					this.x[this.i] = (-this.b[this.i]);
					this.varType[m] = 2;
					this.ArtificialAdded = true;
					this.BasicVariables[this.i] = m;

					int i1 = arrayOfInt[this.i];
					this.NonBasicVariables[n] = i1;

					n++;
					m++;
					this.NumArtificials += 1;
				}
				break;
			}
		}
		this.numNonbasic = (m - this.numConstraints);
		this.numVariables = m;

		if (this.NumArtificials > 0) {
			setCostForPhaseOne();
		}
		return true;
	}

	public void reset(int paramInt1, int paramInt2) {
		for (int m = 0; m < this.numConstraints; m++) {
			for (int n = 0; n < this.numVariables; n++)
				this.A[m][n] = 0.0F;
		}
		this.numConstraintsSoFar = 0;
		this.numVariables = paramInt1;
		this.objectiveValue = 0.0F;
		this.CurrentStep = 0;
		this.ArtificialAdded = false;
		this.NumArtificials = 0;
	}

	public void showInfo() {
		for (int m = 0; m < this.numVariables; m++)
			System.out.println("cost[" + m + "]:" + this.cost[m]);
		for (int n = 0; n < this.numConstraints; n++) {
			for (int i1 = 0; i1 < this.numVariables; i1++)
				System.out.println("A[" + n + "][" + i1 + "]:" + this.A[n][i1]);
		}
		System.out.println("LeavingVariable:" + this.LeavingVariable);

		System.out.println("EnteringVariable:" + this.EnteringVariable);

		for (int i1 = 0; i1 < this.numConstraints; i1++) {
			System.out.println("littleCost[" + i1 + "]:" + this.littleCost[i1]);
		}

		System.out.println("MinRatio:" + this.MinRatio);

		for (int i2 = 0; i2 < this.numConstraints; i2++) {
			System.out.println("pi[" + i2 + "]:" + this.pi[i2]);
		}
		for (int i3 = 0; i3 < this.numConstraints; i3++) {
			System.out.println("yB[" + i3 + "]:" + this.yB[i3]);
		}

		for (int i4 = 0; i4 < this.numConstraints; i4++) {
			System.out.println("BasicVariables[" + i4 + "]:"
					+ this.BasicVariables[i4]);
		}
		for (int i5 = 0; i5 < this.numNonbasic; i5++) {
			System.out.println("NonBasicVariables[" + i5 + "]:"
					+ this.NonBasicVariables[i5]);
		}
		for (int i6 = 0; i6 < this.numConstraints; i6++) {
			System.out.println("x[" + i6 + "]:" + this.x[i6]);
		}
		for (int i7 = 0; i7 < this.numNonbasic; i7++) {
			System.out.println("reducedCost[" + i7 + "]:"
					+ this.reducedCost[i7]);
		}
		System.out.println("objectiveValue:" + this.objectiveValue);

		for (int i8 = 0; i8 < this.numVariables; i8++) {
			System.out.println("OriginalCost[" + i8 + "]:"
					+ this.OriginalCost[i8]);
		}
		System.out.println("TypeMinimize" + this.TypeMinimize);
		System.out.println("oldOptType" + this.oldOptType);
	}

	public int solveLP() {
		int m = 0;

		while (m == 0) {
			m = iterate();
		}
		return m;
	}

	public void specifyObjective(float[] paramArrayOfFloat, boolean paramBoolean) {
		for (this.i = 0; this.i < this.numVariables; this.i += 1) {
			this.cost[this.i] = paramArrayOfFloat[this.i];
		}
		this.TypeMinimize = paramBoolean;
	}

	public boolean testForOptimality() {
		boolean bool = true;

		for (int m = 0; m < this.numNonbasic; m++) {
			if (this.reducedCost[m] < 0.0F) {
				bool = false;
				return bool;
			}
		}
		return bool;
	}

	public boolean testUnboundedness() {
		boolean bool = true;

		for (this.i = 0; this.i < this.numConstraints; this.i += 1)
			if (this.yB[this.i] > 0.0F) {
				bool = false;
				break;
			}
		return bool;
	}

	public void updateSolution() {
		for (this.i = 0; this.i < this.numConstraints; this.i += 1)
			this.x[this.i] -= this.MinRatio * this.yB[this.i];
		this.x[this.LeavingVariable] = this.MinRatio;

		if (this.varType[this.BasicVariables[this.LeavingVariable]] == 2)
			this.NumArtificials -= 1;
		if (this.varType[this.NonBasicVariables[this.EnteringVariable]] == 2) {
			this.NumArtificials += 1;
		}
		int m = this.BasicVariables[this.LeavingVariable];
		this.BasicVariables[this.LeavingVariable] = this.NonBasicVariables[this.EnteringVariable];
		this.NonBasicVariables[this.EnteringVariable] = m;
	}
}