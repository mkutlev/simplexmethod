package iopp.simplex;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;

public class ColorPanel extends Panel {
	Label[] numbers;
	int rows;
	int cols;

	public ColorPanel(int paramInt, Color paramColor) {
		this.rows = paramInt;
		setLayout(new GridLayout(paramInt, 1));

		setBackground(paramColor);

		this.numbers = new Label[paramInt];

		for (int i = 0; i < paramInt; i++) {
			this.numbers[i] = new Label("        ");
			add(this.numbers[i]);
		}
	}
}