package iopp.simplex;

import java.awt.Button;
import java.awt.Dimension;
import java.awt.Event;
import java.awt.Frame;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.Toolkit;

public class DimensionFrame extends Frame {
	TextField numConstraints;
	TextField numVariables;
	Button ok;
	Panel top;
	Panel buttonPanel;
	Label reminderCon;
	Label reminderVar;
	SimplexTool outerParent;
	GridBagLayout gridbag;

	public DimensionFrame(SimplexTool paramSimplexTool, String paramString) {
		super(paramString);
		this.outerParent = paramSimplexTool;

		this.gridbag = new GridBagLayout();

		this.ok = new Button("Continue");

		this.buttonPanel = new Panel();
		this.buttonPanel.setLayout(this.gridbag);

		this.outerParent.constrain(this.buttonPanel, this.ok, 0, 0, 1, 1, 0,
				10, 0.3D, 0.0D, 5, 0, 5, 0);

		this.numConstraints = new TextField(3);
		this.numVariables = new TextField(3);

		this.numConstraints.preferredSize(3);
		this.numVariables.preferredSize(3);

		this.reminderCon = new Label("Between 2 and 7");
		this.reminderVar = new Label("Between 2 and 7");

		this.top = new Panel();

		this.top.setLayout(this.gridbag);

		this.outerParent.constrain(this.top,
				new Label("Number of Constraints"), 0, 0, 1, 1);
		this.outerParent.constrain(this.top, this.reminderCon, 0, 1, 1, 1);
		this.outerParent.constrain(this.top, this.numConstraints, 1, 1, 1, 1);

		this.outerParent.constrain(this.top, new Label("Number of Variables"),
				0, 2, 1, 1);
		this.outerParent.constrain(this.top, this.reminderVar, 0, 3, 1, 1);
		this.outerParent.constrain(this.top, this.numVariables, 1, 3, 1, 1);

		setLayout(new GridBagLayout());

		this.outerParent.constrain(this, this.top, 0, 0, 1, 1, 1, 11, 0.0D,
				0.0D, 10, 5, 10, 5);
		this.outerParent.constrain(this, this.buttonPanel, 0, 1, 1, 1, 0, 10,
				0.0D, 0.0D, 10, 5, 10, 5);

		// set the size of the frame (applet will be width by height in size)
		int height = 150;
		int width = 200;
		// center the frame on screen
		Dimension sD = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation((sD.width - width) / 2, (sD.height - height) / 2);
	}

	public boolean action(Event paramEvent, Object paramObject) {
		if ((paramEvent.target instanceof Button)) {
			this.outerParent.update(this);
			return true;
		}

		return false;
	}
}
