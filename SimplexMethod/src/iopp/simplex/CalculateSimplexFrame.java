package iopp.simplex;

import java.awt.Button;
import java.awt.Checkbox;
import java.awt.CheckboxGroup;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Event;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextArea;
import java.awt.Toolkit;

public class CalculateSimplexFrame extends Frame {
	SimplexTool outerParent;
	RevisedSimplex LP;
	Panel ObjPanel;
	Panel UserObjFunc;
	Panel ObjFunc;
	Label VarLabelObj;
	Panel Apanel;
	Panel ReducedCostPanel;
	Label ReducedCostLabel = new Label("The Reduced Costs");
	Label[] RedCosts;
	CheckboxGroup usersChoice;
	Checkbox[] whichVariable;
	Panel Bpanel;
	ColorPanel[] Acoefficients;
	Label[][] Bcoefficients;
	Panel ObjValuePanel;
	float[] coefficients;
	Panel MessagePanel;
	TextArea Messages;
	Panel legend;
	Label basic = new Label(" Basic Variables ");
	Label slack = new Label(" Slack/Surplus Variable ");
	Label artificial = new Label(" Artificial Variable ");
	Label entering = new Label(" Entering Variable ");
	Label leaving = new Label(" Leaving Variable ");
	Panel otherStuff;
	ColorPanel yB;
	ColorPanel pi;
	ColorPanel x;
	Label Value;
	ColorPanel littleCost;
	ColorPanel rhs;
	Panel ButtonPanel;
	Button step = new Button("Next Operation");
	Button iterate = new Button("Do A Full Iterate");
	Button quit = new Button("Quit");

	public CalculateSimplexFrame(SimplexTool paramSimplexTool,
			String paramString, RevisedSimplex paramrevisedSimplex) {
		super(paramString);
		this.outerParent = paramSimplexTool;
		setLayout(new GridBagLayout());

		this.LP = paramrevisedSimplex;

		this.coefficients = new float[this.LP.numVariables];

		makePanels();

		this.outerParent.constrain(this, this.ObjPanel, 0, 0, 1, 1, 2, 18,
				1.0D, 1.0D, 5, 5, 5, 5);
		this.outerParent.constrain(this, this.Apanel, 0, 1, 1, 1, 2, 18, 1.0D,
				1.0D, 5, 5, 5, 5);
		this.outerParent.constrain(this, this.ReducedCostPanel, 0, 2, 1, 1, 2,
				18, 1.0D, 1.0D, 5, 5, 5, 5);
		this.outerParent.constrain(this, this.otherStuff, 0, 3, 1, 1, 2, 18,
				1.0D, 1.0D, 5, 5, 5, 5);
		this.outerParent.constrain(this, this.ObjValuePanel, 0, 4, 1, 1, 2, 18,
				1.0D, 1.0D, 5, 5, 5, 5);
		this.outerParent.constrain(this, this.MessagePanel, 0, 5, 1, 1, 2, 18,
				1.0D, 1.0D, 5, 5, 5, 5);
		this.outerParent.constrain(this, this.ButtonPanel, 0, 6, 1, 1, 2, 18,
				1.0D, 1.0D, 5, 5, 5, 5);
		this.outerParent.constrain(this, this.legend, 0, 7, 1, 1, 2, 18, 1.0D,
				1.0D, 5, 5, 5, 5);
		pack();

		// set the size of the frame (applet will be width by height in size)
		int height = 600;
		int width = 500;
		// center the frame on screen
		Dimension sD = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation((sD.width - width) / 2, (sD.height - height) / 2);

		show();
	}

	public boolean action(Event paramEvent, Object paramObject) {
		boolean bool1 = true;
		boolean bool2 = false;

		if ((paramEvent.target instanceof Button)) {
			if (paramEvent.target == this.step) {
				this.iterate.disable();
				this.outerParent.update(this, bool1);
				return true;
			}

			if (paramEvent.target == this.iterate) {
				this.outerParent.update(this, bool2);
				return true;
			}

			if (paramEvent.target == this.quit) {
				if (!this.outerParent.start.isEnabled())
					this.outerParent.start.enable();

				this.outerParent.activeCruncher.dispose();

				return true;
			}
		}
		return false;
	}

	void makePanels() {
		Label[] arrayOfLabel = new Label[this.LP.numVariables];

		this.ObjPanel = new Panel();
		this.ObjPanel.setLayout(new GridLayout(4, 1));

		if (this.LP.oldOptType == true)
			this.ObjPanel.add(new Label("Your Objective: Minimize"));
		else {
			this.ObjPanel.add(new Label("Your Objective: Maximize"));
		}
		this.UserObjFunc = new Panel();
		this.UserObjFunc.setLayout(new FlowLayout(0));

		for (int i = 0; i < this.outerParent.numVariables; i++) {
			if ((this.outerParent.dataFrame.objective[i].getFloat() < 0.0D)
					|| (i == 0))
				this.UserObjFunc.add(new Label(" "
						+ this.outerParent.dataFrame.objective[i].getFloat()
						+ " x" + (i + 1)));
			else {
				this.UserObjFunc.add(new Label("+ "
						+ this.outerParent.dataFrame.objective[i].getFloat()
						+ " x" + (i + 1)));
			}
		}
		this.ObjPanel.add(this.UserObjFunc);

		this.ObjPanel.add(new Label("Preprocessed Objective: Minimize"));

		this.ObjFunc = new Panel();
		this.ObjFunc.setLayout(new FlowLayout(0));

		arrayOfLabel = new Label[this.LP.numVariables];

		for (int i = 0; i < this.LP.numVariables; i++) {
			arrayOfLabel[i] = new Label();
			if ((this.LP.cost[i] < 0.0D) || (i == 0))
				arrayOfLabel[i].setText(" " + Float.toString(this.LP.cost[i])
						+ " x" + (i + 1));
			else {
				arrayOfLabel[i].setText("+ " + Float.toString(this.LP.cost[i])
						+ " x" + (i + 1));
			}
			switch (this.LP.varType[i]) {
			case 0:
				arrayOfLabel[i].setBackground(Color.lightGray);
				break;
			case 1:
				arrayOfLabel[i].setBackground(Color.cyan);
				break;
			case 2:
				arrayOfLabel[i].setBackground(Color.orange);
			}

			this.ObjFunc.add(arrayOfLabel[i]);
		}

		this.ObjPanel.add(this.ObjFunc);

		this.Apanel = new Panel();
		this.Apanel.setLayout(new GridBagLayout());

		this.Acoefficients = new ColorPanel[this.LP.numVariables];
		this.outerParent.constrain(this.Apanel,
				new Label("Constraint Matrix:"), 0, 0, this.LP.numVariables, 1);
		int j;
		for (int i = 0; i < this.LP.numVariables; i++) {
			this.Acoefficients[i] = new ColorPanel(this.LP.numConstraints,
					Color.lightGray);

			for (j = 0; j < this.LP.numConstraints; j++) {
				this.Acoefficients[i].numbers[j].setText(Float
						.toString(this.LP.A[j][i]) + "     ");

				this.outerParent.constrain(this.Apanel, this.Acoefficients[i],
						i, 1, 1, 1);
			}
		}

		for (int i = 0; i < this.LP.numConstraints; i++) {
			this.Acoefficients[this.LP.BasicVariables[i]]
					.setBackground(Color.red);
		}

		for (int i = 0; i < this.LP.numNonbasic; i++) {
			this.Acoefficients[this.LP.NonBasicVariables[i]]
					.setBackground(Color.lightGray);
		}

		this.rhs = new ColorPanel(this.LP.numConstraints, Color.white);

		for (int i = 0; i < this.LP.numConstraints; i++) {
			this.rhs.numbers[i].setText(Float.toString(this.LP.b[i]));
		}
		this.outerParent.constrain(this.Apanel, new Label("RHS"),
				this.LP.numVariables, 0, 1, 1, 0, 10, 0, 0);
		this.outerParent.constrain(this.Apanel, this.rhs, this.LP.numVariables,
				1, 1, 4, 0, 10, 0, 0);

		this.ReducedCostPanel = new Panel();
		this.ReducedCostPanel.setLayout(new GridBagLayout());

		this.outerParent.constrain(this.ReducedCostPanel,
				this.ReducedCostLabel, 0, 0, this.LP.numVariables, 1, 5, 5, 5,
				5);

		this.usersChoice = new CheckboxGroup();
		this.whichVariable = new Checkbox[this.LP.numVariables];
		this.RedCosts = new Label[this.LP.numVariables];

		for (int i = 0; i < this.LP.numVariables; i++) {
			this.RedCosts[i] = new Label("             ");
			this.whichVariable[i] = new Checkbox("x" + (i + 1),
					this.usersChoice, false);
			this.whichVariable[i].disable();
			this.outerParent.constrain(this.ReducedCostPanel, this.RedCosts[i],
					i, 1, 1, 1, 2, 17, 1.0D, 1.0D, 0, 0, 0, 0);

			this.outerParent.constrain(this.ReducedCostPanel,
					this.whichVariable[i], i, 2, 1, 1, 2, 17, 1.0D, 1.0D, 0, 0,
					0, 0);
		}

		this.otherStuff = new Panel();
		this.otherStuff.setLayout(new GridBagLayout());
		this.outerParent.constrain(this.otherStuff, new Label("x"), 0, 0, 1, 1,
				0, 0, 0, 2);
		this.x = new ColorPanel(this.LP.numConstraints, Color.magenta);

		this.outerParent.constrain(this.otherStuff, this.x, 0, 1, 1, 1, 2, 17,
				1.0D, 1.0D, 0, 0, 0, 2);

		for (int i = 0; i < this.LP.numConstraints; i++) {
			this.x.numbers[i].setText("x" + (this.LP.BasicVariables[i] + 1)
					+ "= " + Float.toString(this.LP.x[i]) + "      ");
		}

		this.outerParent.constrain(this.otherStuff, new Label("cB"), 1, 0, 1,
				1, 0, 2, 0, 2);
		this.littleCost = new ColorPanel(this.LP.numConstraints, Color.green);
		for (int i = 0; i < this.LP.numConstraints; i++)
			this.littleCost.numbers[i].setText(" "
					+ Float.toString(this.LP.cost[this.LP.BasicVariables[i]])
					+ "   ");
		this.outerParent.constrain(this.otherStuff, this.littleCost, 1, 1, 1,
				1, 2, 17, 1.0D, 1.0D, 0, 2, 0, 2);

		this.outerParent.constrain(this.otherStuff, new Label("yB"), 2, 0, 1,
				1, 2, 17, 1.0D, 1.0D, 0, 2, 0, 2);
		this.yB = new ColorPanel(this.LP.numConstraints, Color.white);
		for (int i = 0; i < this.LP.numConstraints; i++)
			this.yB.numbers[i].setText("            ");
		this.outerParent.constrain(this.otherStuff, this.yB, 2, 1, 1, 1, 2, 17,
				1.0D, 1.0D, 0, 2, 0, 2);

		this.outerParent.constrain(this.otherStuff, new Label("pi"), 3, 0, 1,
				1, 0, 2, 0, 2);
		this.pi = new ColorPanel(this.LP.numConstraints, Color.lightGray);
		for (int i = 0; i < this.LP.numConstraints; i++)
			this.pi.numbers[i].setText("            ");
		this.outerParent.constrain(this.otherStuff, this.pi, 3, 1, 1, 1, 2, 17,
				1.0D, 1.0D, 0, 2, 0, 2);

		this.Bpanel = new Panel();
		this.Bpanel.setLayout(new GridBagLayout());

		this.Bcoefficients = new Label[this.LP.numConstraints][this.LP.numConstraints];
		this.outerParent.constrain(this.Bpanel, new Label("The B matrix."), 0,
				0, this.LP.numConstraints, 1);

		for (int i = 0; i < this.LP.numConstraints; i++) {
			this.Bcoefficients[i] = new Label[this.LP.numConstraints];

			for (j = 0; j < this.LP.numConstraints; j++) {
				this.Bcoefficients[i][j] = new Label("        ");
				this.Bcoefficients[i][j].setBackground(Color.red);

				this.outerParent.constrain(this.Bpanel,
						this.Bcoefficients[i][j], j, i + 1, 1, 1);
			}
		}

		this.outerParent.constrain(this.otherStuff, this.Bpanel, 4, 0, 1, 4, 0,
				10, 0, 0);

		this.ObjValuePanel = new Panel();
		this.Value = new Label();
		this.Value.setText("                       ");

		this.ObjValuePanel.add(new Label("Current Objective Value:"));
		this.ObjValuePanel.add(this.Value);

		this.MessagePanel = new Panel();
		this.Messages = new TextArea(3, 40);
		this.Messages.setEditable(false);

		if (this.outerParent.LP.NumArtificials > 0)
			this.Messages
					.setText("Artificial Variable Added\nUsing Two Phase Method.");
		else {
			this.Messages.setText("Ready!");
		}
		this.MessagePanel.add(new Label("Messages:"));
		this.MessagePanel.add(this.Messages);

		this.legend = new Panel();
		this.legend.setLayout(new GridLayout(2, 3));
		this.basic.setBackground(Color.red);
		this.slack.setBackground(Color.cyan);
		this.artificial.setBackground(Color.orange);
		this.entering.setBackground(Color.yellow);
		this.leaving.setBackground(Color.white);

		this.legend.add(new Label("Color Legend"));
		this.legend.add(this.basic);
		this.legend.add(this.slack);
		this.legend.add(this.artificial);
		this.legend.add(this.entering);
		this.legend.add(this.leaving);

		this.ButtonPanel = new Panel();
		this.ButtonPanel.setLayout(new FlowLayout(1));

		this.ButtonPanel.add(this.step);
		this.ButtonPanel.add(this.iterate);
		this.ButtonPanel.add(this.quit);
	}

	public void updateAllPanels() {
		for (int i = 0; i < this.LP.numNonbasic; i++) {
			this.RedCosts[this.LP.NonBasicVariables[i]].setText(Float
					.toString(this.LP.reducedCost[i]));
		}
		for (int i = 0; i < this.LP.numConstraints; i++) {
			this.RedCosts[this.LP.BasicVariables[i]].setText("Basic");
		}
		if (this.outerParent.SolveStatus != 1) {
			this.RedCosts[this.LP.BasicVariables[this.LP.LeavingVariable]]
					.setText(Float
							.toString(this.LP.reducedCost[this.LP.EnteringVariable]));

			this.RedCosts[this.LP.NonBasicVariables[this.LP.EnteringVariable]]
					.setText("        ");
		}

		for (int i = 0; i < this.LP.numConstraints; i++) {
			this.x.numbers[i].setText("x" + (this.LP.BasicVariables[i] + 1)
					+ "= " + Float.toString(this.LP.x[i]));
		}
		for (int i = 0; i < this.LP.numConstraints; i++) {
			this.pi.numbers[i].setText(" " + Float.toString(this.LP.pi[i]));
		}
		for (int i = 0; i < this.LP.numConstraints; i++) {
			this.littleCost.numbers[i].setText(" "
					+ Float.toString(this.LP.cost[this.LP.BasicVariables[i]]));
		}
		for (int i = 0; i < this.LP.numConstraints; i++) {
			this.yB.numbers[i].setText(" " + Float.toString(this.LP.yB[i]));
		}
		for (int i = 0; i < this.LP.numConstraints; i++) {
			for (int j = 0; j < this.LP.numConstraints; j++)
				this.Bcoefficients[j][i].setText(Float
						.toString(this.LP.A[j][this.LP.BasicVariables[i]]));
		}
		this.Value.setText(" " + Float.toString(this.LP.calculateObjective()));

		for (int i = 0; i < this.LP.numConstraints; i++) {
			this.Acoefficients[this.LP.BasicVariables[i]]
					.setBackground(Color.red);
		}

		for (int i = 0; i < this.LP.numNonbasic; i++)
			this.Acoefficients[this.LP.NonBasicVariables[i]]
					.setBackground(Color.lightGray);
	}

	public void updatePanelsForOneStep() {
		int i = this.LP.CurrentStep;
		int k;
		switch (i) {
		case 1:
			for (k = 0; k < this.LP.numConstraints; k++)
				for (int m = 0; m < this.LP.numConstraints; m++)
					this.Bcoefficients[m][k].setText(Float
							.toString(this.LP.A[m][this.LP.BasicVariables[k]]));
			this.outerParent.activeCruncher.Messages.setText("Made B.");
			break;
		case 2:
			for (k = 0; k < this.LP.numConstraints; k++)
				this.pi.numbers[k].setText(" " + Float.toString(this.LP.pi[k]));
			this.outerParent.activeCruncher.Messages
					.setText("Calculated B^T pi = cB.");
			break;
		case 3:
			for (k = 0; k < this.LP.numNonbasic; k++) {
				this.RedCosts[this.LP.NonBasicVariables[k]].setText(Float
						.toString(this.LP.reducedCost[k]));
			}

			for (k = 0; k < this.LP.numConstraints; k++) {
				this.RedCosts[this.LP.BasicVariables[k]].setText("basic");
			}

			this.outerParent.activeCruncher.Messages
					.setText("Calculated reduced costs.\nTest for Optimality.");
			break;
		case 4:
			this.outerParent.activeCruncher.Messages
					.setText("Not Optimal!\nPick entering Variable");

			for (k = 0; k < this.LP.numNonbasic; k++) {
				if (this.LP.reducedCost[k] < 0.0F)
					this.outerParent.activeCruncher.whichVariable[this.LP.NonBasicVariables[k]]
							.enable();
			}
			this.outerParent.activeCruncher.whichVariable[this.LP.NonBasicVariables[this.LP.EnteringVariable]]
					.setState(true);

			for (k = 0; k < this.LP.numConstraints; k++) {
				this.outerParent.activeCruncher.whichVariable[this.LP.BasicVariables[k]]
						.setState(false);

				this.outerParent.activeCruncher.whichVariable[this.LP.BasicVariables[k]]
						.disable();
			}
			break;
		case 5:
			for (k = 0; k < this.LP.numNonbasic; k++) {
				if (this.outerParent.activeCruncher.whichVariable[this.LP.NonBasicVariables[k]]
						.getState() == true) {
					this.LP.EnteringVariable = k;
				}
				this.outerParent.activeCruncher.whichVariable[this.LP.NonBasicVariables[k]]
						.setState(false);

				this.outerParent.activeCruncher.whichVariable[this.LP.NonBasicVariables[k]]
						.disable();
			}

			this.outerParent.activeCruncher.Messages
					.setText("The entering Variable is x"
							+ Integer
									.toString(this.LP.NonBasicVariables[this.LP.EnteringVariable] + 1));

			this.Acoefficients[this.LP.NonBasicVariables[this.LP.EnteringVariable]]
					.setBackground(Color.yellow);
			break;
		case 6:
			for (k = 0; k < this.LP.numConstraints; k++)
				this.yB.numbers[k].setText(" " + Float.toString(this.LP.yB[k]));
			this.outerParent.activeCruncher.Messages
					.setText("Calculate Search Direction (yB).\nTest for Unboundedness");
			break;
		case 7:
			this.outerParent.activeCruncher.Messages
					.setText("The Problem is Unbounded");
			break;
		case 8:
			this.outerParent.activeCruncher.Messages
					.setText("The Problem is Not Unbounded.\nWork continues.");
			break;
		case 9:
			this.ReducedCostLabel.setText("Min Ratio");

			for (k = 0; k < this.LP.numConstraints; k++) {
				int j = this.LP.BasicVariables[k];

				if ((this.LP.yB[k] > 0.0F)
						&& (this.LP.x[k] / this.LP.yB[k] == this.LP.MinRatio)) {
					this.outerParent.activeCruncher.whichVariable[j].enable();
					this.RedCosts[j].setText(Float.toString(this.LP.MinRatio));
				} else {
					this.outerParent.activeCruncher.whichVariable[j].disable();
					this.RedCosts[j].setText("   ");
				}

			}

			int j = this.LP.BasicVariables[this.LP.LeavingVariable];
			this.outerParent.activeCruncher.whichVariable[j].setState(true);

			for (k = 0; k < this.LP.numNonbasic; k++) {
				j = this.LP.NonBasicVariables[k];
				this.outerParent.activeCruncher.whichVariable[j]
						.setState(false);
				this.outerParent.activeCruncher.whichVariable[j].disable();
				this.RedCosts[j].setText("   ");
			}

			this.outerParent.activeCruncher.Messages
					.setText("More than one variable has minimum ratio.\nSelect leaving variable.");
			break;
		case 10:
			for (k = 0; k < this.LP.numConstraints; k++) {
				if (this.outerParent.activeCruncher.whichVariable[this.LP.BasicVariables[k]]
						.getState() == true) {
					this.LP.LeavingVariable = k;
				}
				this.outerParent.activeCruncher.whichVariable[this.LP.BasicVariables[k]]
						.setState(false);

				this.outerParent.activeCruncher.whichVariable[this.LP.BasicVariables[k]]
						.disable();
			}

			this.outerParent.activeCruncher.Messages
					.setText("The leaving Variable is x"
							+ Integer
									.toString(this.LP.BasicVariables[this.LP.LeavingVariable] + 1));

			this.ReducedCostLabel.setText("The Reduced Costs");

			for (k = 0; k < this.LP.numVariables; k++) {
				this.RedCosts[k].setText("   ");
			}
			break;
		case 11:
			this.outerParent.activeCruncher.Messages
					.setText("The Min Ratio Test Indicates x"
							+ Integer
									.toString(this.LP.BasicVariables[this.LP.LeavingVariable] + 1)
							+ "\nshould leave the basis.");

			this.Acoefficients[this.LP.BasicVariables[this.LP.LeavingVariable]]
					.setBackground(Color.white);
			break;
		case 12:
			this.outerParent.activeCruncher.Messages
					.setText("Solution found.\nFound X and objective value.");
			break;
		case 0:
			for (k = 0; k < this.LP.numConstraints; k++) {
				this.Acoefficients[this.LP.BasicVariables[k]]
						.setBackground(Color.red);
			}

			for (k = 0; k < this.LP.numNonbasic; k++) {
				this.Acoefficients[this.LP.NonBasicVariables[k]]
						.setBackground(Color.lightGray);
			}

			this.Value.setText(Float.toString(this.LP.calculateObjective()));

			for (k = 0; k < this.LP.numConstraints; k++) {
				this.x.numbers[k].setText("x" + (this.LP.BasicVariables[k] + 1)
						+ "= " + Float.toString(this.LP.x[k]));
			}
			for (k = 0; k < this.LP.numConstraints; k++) {
				this.littleCost.numbers[k]
						.setText(" "
								+ Float.toString(this.LP.cost[this.LP.BasicVariables[k]]));
			}
			this.outerParent.activeCruncher.Messages
					.setText("Update Basis, X,\nand Objective Value.");

			break;
		}
	}
}
