package iopp.simplex;

import java.awt.Button;
import java.awt.Choice;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Event;
import java.awt.Frame;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.Toolkit;

public class EnterDataFrame extends Frame {
	SimplexTool outerParent;
	RevisedSimplex LP;
	Choice minmax;
	Panel dataPanel;
	Panel buttonbar;
	Panel messagePanel;
	Panel objValue = new Panel();

	TextField Messages = new TextField(40);
	int numVariables;
	int numConstraints;
	Button preprocess = new Button("Preprocess");
	Button reset = new Button("Reset");
	Button clear = new Button("Clear");
	Label nonNegativeLabel;
	NumberTextField[][] constraint;
	NumberTextField[] objective;
	NumberTextField[] rhs;
	Choice[] rowType;
	GridBagLayout gridbag = new GridBagLayout();

	public EnterDataFrame(SimplexTool paramSimplexTool, String paramString,
			int paramInt1, int paramInt2) {
		super(paramString);
		this.outerParent = paramSimplexTool;

		this.numVariables = paramInt1;
		this.numConstraints = paramInt2;

		int k = 2 * paramInt1 + 2;

		this.dataPanel = new Panel();
		this.dataPanel.setLayout(this.gridbag);

		this.minmax = new Choice();
		this.minmax.addItem("Minimize");
		this.minmax.addItem("Maximize");

		this.objective = new NumberTextField[paramInt1];

		for (int j = 0; j < paramInt1; j++) {
			this.objective[j] = new NumberTextField();
		}
		this.rowType = new Choice[paramInt2];

		this.outerParent.constrain(this.dataPanel, this.minmax, 0, 0, k, 1, 10,
				0, 10, 0);

		for (int j = 0; j < paramInt1 - 1; j++) {
			this.outerParent.constrain(this.dataPanel, this.objective[j],
					2 * j, 1, 1, 1, 2, 17, 1.0D, 0.0D, 0, 0, 0, 0);
			this.outerParent.constrain(this.dataPanel, new Label("x" + (j + 1)
					+ "+", 0), 2 * j + 1, 1, 1, 1, 0, 17, 0.0D, 0.0D, 0, 0, 0,
					0);
		}

		int j = paramInt1 - 1;
		this.outerParent.constrain(this.dataPanel, this.objective[j], 2 * j, 1,
				1, 1, 2, 17, 1.0D, 0.0D, 0, 0, 0, 0);

		this.outerParent.constrain(this.dataPanel, new Label("x" + (j + 1), 0),
				2 * j + 1, 1, 1, 1, 0, 17, 0.0D, 0.0D, 0, 0, 0, 0);

		this.outerParent.constrain(this.dataPanel, new Label("Subject to:", 0),
				0, 2, k, 1, 0, 17, 0.0D, 0.0D, 10, 0, 10, 0);

		this.constraint = new NumberTextField[paramInt2][paramInt1];
		this.rhs = new NumberTextField[paramInt2];

		for (int i = 0; i < paramInt2; i++) {
			this.rhs[i] = new NumberTextField();

			for (j = 0; j < paramInt1 - 1; j++) {
				this.constraint[i][j] = new NumberTextField();

				this.outerParent.constrain(this.dataPanel,
						this.constraint[i][j], 2 * j, i + 3, 1, 1, 2, 17, 1.0D,
						0.0D, 0, 0, 0, 0);
				this.outerParent.constrain(this.dataPanel, new Label("x"
						+ (j + 1) + "+", 0), 2 * j + 1, i + 3, 1, 1, 0, 17,
						0.0D, 0.0D, 0, 0, 0, 0);
			}

			j = paramInt1 - 1;
			this.constraint[i][j] = new NumberTextField();

			this.outerParent.constrain(this.dataPanel, this.constraint[i][j],
					2 * j, i + 3, 1, 1, 2, 17, 1.0D, 0.0D, 0, 0, 0, 0);

			this.outerParent.constrain(this.dataPanel, new Label("x" + (j + 1),
					0), 2 * j + 1, i + 3, 1, 1, 0, 17, 0.0D, 0.0D, 0, 0, 0, 0);

			this.rowType[i] = new Choice();
			this.rowType[i].addItem("<=");
			this.rowType[i].addItem(">=");
			this.rowType[i].addItem("=");

			this.outerParent.constrain(this.dataPanel, this.rowType[i],
					2 * paramInt1, i + 3, 1, 1);

			this.outerParent.constrain(this.dataPanel, this.rhs[i],
					2 * paramInt1 + 1, i + 3, 1, 1, 2, 17, 1.0D, 0.0D, 0, 0, 0,
					0);
		}

		this.nonNegativeLabel = new Label("x >= 0");
		this.nonNegativeLabel.setBackground(Color.white);

		this.buttonbar = new Panel();
		this.buttonbar.setLayout(this.gridbag);

		this.outerParent.constrain(this.buttonbar, this.preprocess, 0, 0, 1, 1,
				0, 10, 0.3D, 0.0D, 0, 0, 0, 0);

		this.outerParent.constrain(this.buttonbar, this.reset, 1, 0, 1, 1, 0,
				10, 0.3D, 0.0D, 0, 0, 0, 0);

		this.outerParent.constrain(this.buttonbar, this.clear, 2, 0, 1, 1, 0,
				10, 0.3D, 0.0D, 0, 0, 0, 0);

		this.messagePanel = new Panel();
		this.messagePanel.setLayout(this.gridbag);

		this.outerParent.constrain(this.messagePanel, new Label("Messages:"),
				0, 0, 1, 1, 0, 17, 0.0D, 0.0D, 0, 0, 0, 0);
		this.outerParent.constrain(this.messagePanel, this.Messages, 1, 0, 1,
				1, 2, 17, 1.0D, 0.0D, 0, 0, 0, 0);

		this.Messages.setEditable(false);
		this.reset.disable();

		setLayout(this.gridbag);

		this.outerParent.constrain(this, this.dataPanel, 0, 0, 2, 1, 1, 11,
				1.0D, 1.0D, 5, 5, 5, 5);
		this.outerParent.constrain(this, this.nonNegativeLabel, 0, 1, 1, 1, 0,
				17, 0.0D, 0.0D, 0, 0, 0, 0);
		this.outerParent.constrain(this, this.Messages, 1, 1, 1, 1, 2, 17,
				1.0D, 0.0D, 0, 0, 0, 0);
		this.outerParent.constrain(this, this.buttonbar, 0, 2, 2, 1, 2, 10,
				1.0D, 0.0D, 10, 0, 10, 0);

		this.LP = paramSimplexTool.LP;

		// set the size of the frame (applet will be width by height in size)
		int height = 300;
		int width = 600;
		// center the frame on screen
		Dimension sD = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation((sD.width - width) / 2, (sD.height - height) / 2);
	}

	void reset() {
		setEditable(true);

		this.LP.reset(this.numVariables, this.numConstraints);
	}

	public void setEditable(boolean paramBoolean) {
		if (paramBoolean)
			this.minmax.enable();
		else {
			this.minmax.disable();
		}
		for (int j = 0; j < this.numVariables; j++) {
			this.objective[j].setEditable(paramBoolean);
		}
		for (int i = 0; i < this.numConstraints; i++)
			for (int j = 0; j < this.numVariables; j++)
				this.constraint[i][j].setEditable(paramBoolean);
	}

	public void clear() {
		for (int j = 0; j < this.numVariables; j++) {
			this.objective[j].setText("");
		}
		for (int i = 0; i < this.numConstraints; i++) {
			for (int j = 0; j < this.numVariables; j++)
				this.constraint[i][j].setText("");
		}
		for (int i = 0; i < this.numConstraints; i++) {
			this.rhs[i].setText("");
		}
		for (int i = 0; i < this.numConstraints; i++) {
			this.rowType[i].select(0);
		}
		this.minmax.select(0);
	}

	public boolean action(Event paramEvent, Object paramObject) {
		if ((paramEvent.target instanceof Button)) {
			if (paramEvent.target == this.preprocess) {
				if (this.outerParent.ReadDataAndPreprocess(
						this.outerParent.numVariables,
						this.outerParent.numConstraints)) {
					this.preprocess.disable();
					this.reset.enable();
					this.clear.disable();
				}

				return true;
			}

			if (paramEvent.target == this.reset) {
				this.outerParent.LP.reset(this.outerParent.numVariables,
						this.outerParent.numConstraints);

				if (this.outerParent.activeCruncher != null) {
					this.outerParent.activeCruncher.dispose();
				}
				if (this.outerParent.activeCruncher.iterate.isEnabled()) {
					this.outerParent.activeCruncher.iterate.disable();
				}
				if (this.outerParent.activeCruncher.step.isEnabled()) {
					this.outerParent.activeCruncher.step.disable();
				}
				this.outerParent.dataFrame.preprocess.enable();
				this.outerParent.dataFrame.clear.enable();
				this.outerParent.dataFrame.reset.disable();

				this.outerParent.dataFrame.reset();
				this.outerParent.dataFrame.Messages
						.setText("Press Preprocess to Begin.");

				return true;
			}

			if (paramEvent.target == this.clear) {
				clear();
				return true;
			}
		}
		return false;
	}
}