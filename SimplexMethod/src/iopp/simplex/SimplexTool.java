package iopp.simplex;

import java.applet.Applet;
import java.awt.Button;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Event;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;

import javax.swing.JFrame;

public class SimplexTool extends Applet {
	int numConstraints;
	int numVariables;
	RevisedSimplex LP;
	DimensionFrame DimensionWindow;
	EnterDataFrame dataFrame;
	CalculateSimplexFrame activeCruncher;
	CalculateSimplexFrame oldCruncher;
	float[] coefficients = new float[33];
	int SolveStatus;
	Button start = new Button("New Problem");
	Button end = new Button("Quit");

	public boolean ReadDataAndPreprocess(int paramInt1, int paramInt2) {
		this.LP.reset(paramInt1, paramInt2);

		for (int i = 0; i < paramInt1; i++) {
			if (this.dataFrame.objective[i].isNumber()) {
				this.coefficients[i] = this.dataFrame.objective[i].getFloat();
			} else {
				this.dataFrame.Messages.setText("Error: Objective x" + (i + 1));
				return false;
			}
		}

		if (this.dataFrame.minmax.getSelectedIndex() == 0)
			this.LP.specifyObjective(this.coefficients, true);
		else {
			this.LP.specifyObjective(this.coefficients, false);
		}

		for (int j = 0; j < paramInt2; j++) {
			for (int k = 0; k < paramInt1; k++)
				if (this.dataFrame.constraint[j][k].isNumber()) {
					this.coefficients[k] = this.dataFrame.constraint[j][k]
							.getFloat();
				} else {
					this.dataFrame.Messages.setText("Error: Constraint "
							+ (j + 1) + " in x" + (k + 1));
					return false;
				}
			float f;
			if (this.dataFrame.rhs[j].isNumber()) {
				f = this.dataFrame.rhs[j].getFloat();
			} else {
				this.dataFrame.Messages.setText("Error: Constraint " + (j + 1)
						+ "RHS");
				return false;
			}

			this.LP.addConstraint(this.coefficients, f,
					this.dataFrame.rowType[j].getSelectedIndex());
		}

		this.LP.preprocess(paramInt1, paramInt2);

		this.dataFrame.setEditable(false);
		this.dataFrame.Messages.setText("Starting Simplex Window");

		if (this.LP.NumArtificials > 0) {
			this.activeCruncher = new CalculateSimplexFrame(this, "Phase 1",
					this.LP);
			this.dataFrame.Messages.setText("Starting phase 1");
		} else {
			this.activeCruncher = new CalculateSimplexFrame(this, "Phase 2",
					this.LP);
			this.dataFrame.Messages.setText("Starting phase 2");
		}

		return true;
	}

	public boolean action(Event paramEvent, Object paramObject) {
		if ((paramEvent.target instanceof Button)) {
			if (paramEvent.target == this.start) {
				if (this.dataFrame != null)
					this.dataFrame.dispose();
				if (this.activeCruncher != null)
					this.activeCruncher.dispose();
				if (this.LP != null)
					this.LP.reset(this.numVariables, this.numConstraints);

				this.DimensionWindow = new DimensionFrame(this,
						"Enter the Sizes");
				this.DimensionWindow.pack();
				this.DimensionWindow.show();
				this.start.disable();
				return true;
			}

			if (paramEvent.target == this.end) {
				if (this.DimensionWindow != null)
					this.DimensionWindow.dispose();
				if (this.dataFrame != null)
					this.dataFrame.dispose();
				if (this.activeCruncher != null)
					this.activeCruncher.dispose();

				if (!this.start.isEnabled())
					this.start.enable();
				return true;
			}

		}

		return false;
	}

	public void constrain(Container paramContainer, Component paramComponent,
			int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
		constrain(paramContainer, paramComponent, paramInt1, paramInt2,
				paramInt3, paramInt4, 0, 18, 0.0D, 0.0D, 0, 0, 0, 0);
	}

	public void constrain(Container paramContainer, Component paramComponent,
			int paramInt1, int paramInt2, int paramInt3, int paramInt4,
			int paramInt5, int paramInt6, double paramDouble1,
			double paramDouble2, int paramInt7, int paramInt8, int paramInt9,
			int paramInt10) {
		GridBagConstraints localGridBagConstraints = new GridBagConstraints();
		localGridBagConstraints.gridx = paramInt1;
		localGridBagConstraints.gridy = paramInt2;
		localGridBagConstraints.gridwidth = paramInt3;
		localGridBagConstraints.gridheight = paramInt4;
		localGridBagConstraints.fill = paramInt5;
		localGridBagConstraints.anchor = paramInt6;
		localGridBagConstraints.weightx = paramDouble1;
		localGridBagConstraints.weighty = paramDouble2;
		if (paramInt7 + paramInt9 + paramInt8 + paramInt10 > 0) {
			localGridBagConstraints.insets = new Insets(paramInt7, paramInt8,
					paramInt9, paramInt10);
		}
		((GridBagLayout) paramContainer.getLayout()).setConstraints(
				paramComponent, localGridBagConstraints);
		paramContainer.add(paramComponent);
	}

	public void constrain(Container paramContainer, Component paramComponent,
			int paramInt1, int paramInt2, int paramInt3, int paramInt4,
			int paramInt5, int paramInt6, int paramInt7, int paramInt8) {
		constrain(paramContainer, paramComponent, paramInt1, paramInt2,
				paramInt3, paramInt4, 0, 18, 0.0D, 0.0D, paramInt5, paramInt6,
				paramInt7, paramInt8);
	}

	public void init() {
		super.init();
		setBackground(Color.white);

		this.start.setBackground(Color.lightGray);
		this.end.setBackground(Color.lightGray);

		add(this.start);
		add(this.end);
	}

	public void update(CalculateSimplexFrame paramnumberCrunchingFrame,
			boolean paramBoolean) {
		if (paramBoolean) {
			this.SolveStatus = this.LP.iterateOneStep();
			this.activeCruncher.updatePanelsForOneStep();

			if (this.LP.CurrentStep == 0)
				paramnumberCrunchingFrame.iterate.enable();
		} else {
			this.activeCruncher.Messages.setText("Keep Iterating");
			this.SolveStatus = this.LP.iterate();
			this.activeCruncher.updateAllPanels();
		}

		switch (this.SolveStatus) {
		case 3:
			this.activeCruncher.Messages.setText("The problem is unbounded.");
			paramnumberCrunchingFrame.iterate.disable();
			paramnumberCrunchingFrame.step.disable();
			this.start.enable();
			break;
		case 1:
			if (this.LP.ArtificialAdded == false) {
				this.activeCruncher.Messages
						.setText("You've Done It!\nYou've Solved It!!!");
				this.dataFrame.Messages.setText("");
				paramnumberCrunchingFrame.updateAllPanels();
				paramnumberCrunchingFrame.iterate.disable();
				paramnumberCrunchingFrame.step.disable();
				this.start.enable();
			} else if (this.LP.calculateObjective() == 0.0F) {
				if (this.LP.NumArtificials > 0) {
					System.out.print("Even though 0 objective, there are");
					System.out.println(" still artificials in basis.");
				}

				System.out.println("Calling getRidOfArtificials");
				this.LP.getRidOfArtificials();
				System.out.println("Called  getRidOfArtificials");

				this.activeCruncher.Messages
						.setText("Artificial Variables Eliminated\nGetting New Window");

				this.oldCruncher = this.activeCruncher;
				this.activeCruncher = new CalculateSimplexFrame(this,
						"Phase 2", this.LP);
				this.oldCruncher.dispose();

				this.activeCruncher.Messages
						.setText("Continuing With Original Problem\n(Phase 2)");
				this.dataFrame.Messages.setText("In phase 2");
			} else {
				this.activeCruncher.Messages
						.setText("The Problem is infeasible.");
				paramnumberCrunchingFrame.iterate.disable();
				paramnumberCrunchingFrame.step.disable();
				this.start.enable();
			}

			break;
		case 2:
		}
	}

	public void update(DimensionFrame paramproblemDimensionWindow) {
		try {
			this.numVariables = Integer.valueOf(
					paramproblemDimensionWindow.numVariables.getText())
					.intValue();
			this.numConstraints = Integer.valueOf(
					paramproblemDimensionWindow.numConstraints.getText())
					.intValue();

			if ((this.numVariables >= 2) && (this.numConstraints >= 2)
					&& (this.numVariables <= 7) && (this.numConstraints <= 7)) {
				paramproblemDimensionWindow.dispose();
				this.LP = new RevisedSimplex(this.numVariables,
						this.numConstraints);
				this.dataFrame = new EnterDataFrame(this,
						"Enter Your Linear Program!", this.numVariables,
						this.numConstraints);
				this.dataFrame.Messages.setText("Press Preprocess to Begin.");
				this.dataFrame.pack();
				this.dataFrame.show();
				this.start.enable();
				// this.dataFrame.setVisible(true);
			} else {
				if ((this.numVariables > 7) || (this.numVariables < 2))
					paramproblemDimensionWindow.reminderVar
							.setBackground(Color.red);
				else {
					paramproblemDimensionWindow.reminderVar
							.setBackground(Color.lightGray);
				}
				if ((this.numConstraints > 7) || (this.numConstraints < 2))
					paramproblemDimensionWindow.reminderCon
							.setBackground(Color.red);
				else
					paramproblemDimensionWindow.reminderCon
							.setBackground(Color.lightGray);
			}
		} catch (NumberFormatException localNumberFormatException) {
		}
	}

	public static void main(String[] args) {
		// create the frame with a title
		JFrame frame = new JFrame("Simplex Method");
		// exit the application when the JFrame is closed
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// add the applet to the frame
		Applet simplexApplet = new SimplexTool();
		simplexApplet.init();
		simplexApplet.start();
		frame.getContentPane().add(simplexApplet);
		// set the size of the frame (applet will be width by height in size)
		int height = 150;
		int width = 200;
		// pack the frame to get correct insets
		frame.pack();
		Insets fI = frame.getInsets();
		frame.setSize(width + fI.right + fI.left, height + fI.top + fI.bottom);
		// center the frame on screen
		Dimension sD = Toolkit.getDefaultToolkit().getScreenSize();
		frame.setLocation((sD.width - width) / 2, (sD.height - height) / 2);
		// make the frame visible
		frame.setVisible(true);
	}

}