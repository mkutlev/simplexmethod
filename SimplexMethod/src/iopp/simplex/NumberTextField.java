package iopp.simplex;

import java.awt.TextField;

public class NumberTextField extends TextField {
	private Float tmp;

	public NumberTextField() {
		super(3);
	}

	public float getFloat() {
		if (this.tmp != null) {
			isNumber();
			return this.tmp.floatValue();
		}

		return this.tmp.floatValue();
	}

	public boolean isNumber() {
		try {
			this.tmp = new Float(Float.valueOf(getText()).floatValue());
		} catch (NumberFormatException localNumberFormatException) {
			if (getText().length() > 0) {
				return false;
			}
			this.tmp = new Float(0.0F);
		}
		return true;
	}
}